
import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/animation_page.dart';
import 'package:componentes/src/pages/avatar_page.dart';
import 'package:componentes/src/pages/card_page.dart';
import 'package:componentes/src/pages/home_page.dart';
import 'package:componentes/src/pages/input_page.dart';
import 'package:componentes/src/pages/list_page.dart';
import 'package:componentes/src/pages/slider_page.dart';
import 'package:flutter/material.dart';
import 'package:componentes/src/pages/home_temp.dart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
       theme: ThemeData(fontFamily: 'Waltograph'),
      debugShowCheckedModeBanner: false,
      //home: HomePage(),
     // home: HomePageTemp(),
     routes: {
       '/': (context) => HomePage(),
       'alert': (context) => AlertPage(),
       'avatar': (context) => AvatarPage(),
       'animated': (context) => AnimationPage(),
       'card': (context) => CardPage(),
       'inputs': (context) => InputPage(),
       'slider': (context) => SlidertPage(),
       'listas': (context) => ListPage(),
     },
    );
  }
}