
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Alertas',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 30),
                RaisedButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context) => AlertDialog(
                        title: Text('Mensaje', style: TextStyle(color: Colors.indigo[900],fontWeight: FontWeight.bold)),
                        content: Text('Este es un cuadro de alerta'),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('Ok'),
                            onPressed: (){
                              Navigator.of(context).pop('Ok');
                            },
                          )
                        ],
                      )
                    );
                  },
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(2.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF3949AB),
                          Color(0xFF3F51B5),
                          Color(0xFFC5CAE9),
                        ],
                      ),
                    ),
                    padding: const EdgeInsets.all(15.0),
                    child:
                        const Text('Mostrar Alerta', style: TextStyle(fontSize: 20)),
                  ),
                ),    
            ],
          ),
        ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}