import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Lista',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
              body: Center(
          child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 200.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Container(
                width: 160.0,
                color: Colors.red,
              ),
              Container(
                width: 160.0,
                color: Colors.blue,
              ),
              Container(
                width: 160.0,
                color: Colors.green,
              ),
              Container(
                width: 160.0,
                color: Colors.yellow,
              ),
              Container(
                width: 160.0,
                color: Colors.orange,
              ),
            ],
          ),
        ),
              ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}