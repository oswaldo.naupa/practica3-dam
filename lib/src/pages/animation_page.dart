
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class AnimationPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
          title: Text('Animaciones',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
          backgroundColor: Color(0xFF3F51B5),
      ),
     body: MyStatefulWidget(),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = !selected;
        });
      },
      child: Center(
        child: AnimatedContainer(
          width: selected ? 200.0 : 100.0,
          height: selected ? 100.0 : 200.0,
          color: selected ? Colors.red : Colors.blue,
          alignment:
              selected ? Alignment.center : AlignmentDirectional.topCenter,
          duration: Duration(seconds: 2),
          curve: Curves.fastOutSlowIn,
          child: FlutterLogo(size: 75),
        ),
      ),
    );
  }
}