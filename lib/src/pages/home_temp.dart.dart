import 'package:flutter/material.dart';


class HomePageTemp extends StatelessWidget {
  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];

  TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: ListView(
        children: _crearItemsCortal()
      ),
    );
  }
  
  List<Widget> _crearItemsCortal() {
    return options.map ((item) {
      return Column(
        children: <Widget>[
          ListTile(title: Text(item), subtitle: Text('Cualquier cosa')),
          Divider()
        ],
      );
    }).toList();
  }

}







/*body: Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Uno', style: styleText),
            Text('Dos', style: styleText),
            Text('Tress', style: styleText),
          ],
        ),
      )*/

 /*body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
          ]
        )
      )*/
/* body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(
              Icons.beach_access,
              color:Colors.blue,
              size: 36.0,
            )
          ],
        ),
      ),*/
/*body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/imagen1.png'),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 36.0,
            ),
          ],
        ),
      ),*/