
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFC5CAE9),
      appBar: AppBar(
        actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Componentes',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: _lista(),
    );
  }

  Widget _lista(){
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
        return ListView(
          children: _listaItems(snapshot.data, context));
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, context){
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = 
    
      ListTile(
        title: Text(opt['texto'], style:TextStyle(color: Color(0xFF212121))),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.arrow_forward_ios, color: Color(0xFFFF5722)),
        onTap: () {
          print(opt['ruta']);
          Navigator.pushNamed(context, opt['ruta']);
        },
      
      );

      opciones..add(widgetTemp) ..add(Divider( color: Color(0xFFBDBDBD),thickness: 3));
     });

     return opciones;
  }
}