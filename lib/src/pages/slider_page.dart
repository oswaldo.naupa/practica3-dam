import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class SlidertPage extends StatefulWidget {
  @override
  createState() {
    return _SliderPageState();
  }
}

class _SliderPageState extends State<SlidertPage> {

 double raiting = 50.0;

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Slider',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                  Slider(
                  activeColor: Colors.indigo[900] ,
                  label: "$raiting",
                  value: raiting,
                  divisions: 20,
                  min: 0,
                  max: 100,
                  onChanged: (newRaiting) {
                    setState(() {
                      raiting = newRaiting;
                    });
                  }
                )
            ],
          ),
        ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}